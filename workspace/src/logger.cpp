#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "Puntuacion.h"

int main(void){
	int fd_fifo, i_read;
	//int puntos1_aux = 0, puntos2_aux = 0, puntos_aux;
	Puntuacion points;
	
	//Creación de la tubería
	if((mkfifo("/tmp/FIFO_LOGGER", 0666)) < 0){ //Tratamiento del error de creación del fifo
		perror("Error al crear la FIFO del LOGGER\n");
		return 1;		
	}
	
	//Apertura de la tubería para leer su contenido
	if ((fd_fifo = open("/tmp/FIFO_LOGGER", O_RDONLY)) < 0){
		perror("Error al abrir la FIFO desde LOGGER.\n");
		return 1;
	}
	
	while(i_read = read(fd_fifo, &points, sizeof(Puntuacion)) == sizeof(Puntuacion)){
		if (points.lastWin == 1){ 
			std::cout << "Jugador 1 marca 1 punto, lleva un total de " << points.puntos1 << " puntos en total." << std::endl;
		}
		
		if (points.lastWin == 2){ 
			std::cout << "Jugador 2 marca 1 punto, lleva un total de " << points.puntos2 << " puntos en total." << std::endl;
		}
	}

	if (i_read < 0){
		if(close(fd_fifo)){
			perror("Error cerrando la FIFO");
		}	
		return 1;
	}
	
	close(fd_fifo);
			
	if(unlink("/tmp/FIFO_LOGGER") < 0){
		perror("Error borrando la FIFO");
		exit(1);
	}
	
	return 0;
}

// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

//Tratamiento del thread
void* hilo_comandos(void* d){ //Rutina con la que se carga el thread con el que se recibirán las teclas pulsadas por el jugador desde el cliente.
	CMundoServidor* p = (CMundoServidor*) d;
	p->RecibeComandosJugador();
}

void CMundoServidor::RecibeComandosJugador(){ //Bucle infinito en el que en cada iteración se lee la tecla pulsada y modifica la velocidad del jugador
	//Apertura de la fifo de CLIENTE a SERVIDOR
	if ((fd_fifo_cliente_servidor = open("/tmp/FIFO_SERVIDOR_CLIENTE", O_RDONLY)) < 0){
		perror("Error al abrir la FIFO_CLIENTE_SERVIDOR desde MundoServidor");
		exit(1);
	}
		
	while(1){
		usleep(10);
		char cad[100];
		read(fd_fifo_cliente_servidor, cad, sizeof(cad));
		unsigned char key;
		sscanf(cad, "%c", &key);
		if(key=='s')jugador1.velocidad.y = -4;
		if(key=='w')jugador1.velocidad.y = +4;
		if(key=='l')jugador2.velocidad.y = -4;
		if(key=='o')jugador2.velocidad.y = +4;
	}
}


CMundoServidor::CMundoServidor()
{
	Init();
}

CMundoServidor::~CMundoServidor()
{
	close(fd_fifo_logger);
	close(fd_fifo_servidor_cliente);
	close(fd_fifo_cliente_servidor);
	
}

void CMundoServidor::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoServidor::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",points.puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",points.puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoServidor::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		
		points.puntos2++;
		points.lastWin = 2;
		write(fd_fifo_logger, &points, sizeof(points));
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;

		points.puntos1++;
		points.lastWin = 1;
		write(fd_fifo_logger, &points, sizeof(points)); //Escribo en la fifo
	}
	time_aux += 0.025f;
	
	//Escritura de los valores de posición en la fifo de comunicación servidor a cliente
	char cad[200];
	sprintf(cad, "%f %f %f %f %f %f %f %f %f %f %d %d",
		esfera.centro.x, esfera.centro.y,
		jugador1.x1, jugador1.y1,
		jugador1.x2, jugador1.y2,
		jugador2.x1, jugador2.y1,
		jugador2.x2, jugador2.y2,
		points.puntos1, points.puntos2);
	if((write(fd_fifo_servidor_cliente, cad, sizeof(cad)))<0){
		perror("Error escribiendo la FIFO_SERVIDOR_CLIENTE desde MundoServidor");
		exit(1);
	}
	
}

void CMundoServidor::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;

	}
	time_aux = 0.0f; //Reinicio la cuenta si el jugador pulsa una tecla.
}

void CMundoServidor::Init()
{
//DIBUJADO DE LA ESCENA
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
	
	time_aux = 0.0f; //Inicio la cuenta del tiempo
	
	//COMUNICACIONES INTERPROCESOS
		// FIFO del logger
	fd_fifo_logger = open("/tmp/FIFO_LOGGER", O_WRONLY);
	if (fd_fifo_logger < 0){
		perror("Error abriendo la FIFO_LOGGER desde MundoServidor.");
		exit(1);
	}
	
		//FIFO de SERVIDOR a CLIENTE
	fd_fifo_servidor_cliente = open("/tmp/FIFO_SERVIDOR_CLIENTE", O_WRONLY);
	if (fd_fifo_servidor_cliente < 0){
		perror("Error leyendo la FIFO_SERVIDOR_CLIENTE desde MundoServidor.");
		exit(1);
	}
	
	//CREACIÓN DE THREAD PARA LA RECEPCIÓN DE DATOS DESDE CLIENTE
	if ((pthread_create(&thid1, NULL, hilo_comandos, this)) != 0){
		perror("Error creando el thread.");
		exit(1);
	}		
}


// Esfera.cpp: implementation of the Esfera class.
//
//////////////////////////////////////////////////////////////////////

#include "Esfera.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Esfera::Esfera()
{
	radio=0.5f;
	velocidad.x=3;
	velocidad.y=3;
	aceleracion.y = 0.0f;
	radio_max = 2.0f;
	radio_min = 0.5f;
	pulso = 0.4f;
}

Esfera::~Esfera()
{

}



void Esfera::Dibuja()
{
	glColor3ub(255,255,0);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

void Esfera::Mueve(float t)
{
	centro = centro + velocidad*t + aceleracion*(0.5f * t * t);
	velocidad = velocidad + aceleracion * t;
	/* //Por razones de optimización, se ha quitado que la esfera sea pulsante.
	if (radio > radio_max) 
		pulso = -pulso; 
	if (radio < radio_min) 
		pulso = -pulso; 

	radio += pulso * t;
	*/
}

void Esfera::setRadio(float r){ //Método de asignación de valor al radio de la esfera
	radio = r;
}

void Esfera::setPos(float x, float y){ //Método de asignación de valor de la posición de la esfera
	centro.x = x;
	centro.y = y;
}

void Esfera::setVel(float vx, float vy){ //Método de asiganción de valor de la velocidad de la esfera
	velocidad.x = vx;
	velocidad.y = vy;
}

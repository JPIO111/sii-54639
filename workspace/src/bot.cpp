#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <sys/mman.h>
#include "DatosMemCompartida.h"
//#include "Esfera.h"
//#include "Raqueta.h"

int main(void){
	int fd;
	DatosMemCompartida *pdata;
	
	fd = open("/tmp/DatosComp", O_RDWR); //Abro el fichero creado por tenis.cpp
	
	pdata = (DatosMemCompartida*)(mmap(NULL, sizeof(DatosMemCompartida), PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0));
	
	if(pdata == MAP_FAILED){
		perror("Error al proyectar en memoria el fichero DatosBot desde bot.cpp");
		exit(1);
	}

	if(close(fd) < 0){
		perror("Error al cerrar fichero DatosBot ");
		return(1);
	}
	
	while(pdata != NULL){
		float centro_raqueta = (pdata->raqueta1.y1 + pdata->raqueta1.y2)/2;
		if((pdata->esfera.centro.y) > centro_raqueta){
			pdata->accion = 1; //Subir
		}
		else if((pdata->esfera.centro.y) < centro_raqueta){
			pdata->accion = -1; //Bajar
		}
		else if((pdata->esfera.centro.y) == centro_raqueta){
			pdata->accion = 0; //Quedarse quieto
		}
		usleep(25000); //Suspende la ejecución del proceso durante 25 ms.
	}
	
	munmap(pdata, sizeof(DatosMemCompartida));
	
	if(unlink("/tmp/DatosComp")<0){
		perror("Error al eliminar DatosBot");
		exit(1);
	}
	return 1;
}

// Esfera.h: interface for the Esfera class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ESFERA_H__8D520BAF_8208_423B_BD91_29F6687FB9C3__INCLUDED_)
#define AFX_ESFERA_H__8D520BAF_8208_423B_BD91_29F6687FB9C3__INCLUDED_


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Vector2D.h"

class Esfera  
{
public:
	float radio;
	float pulso;
	float radio_max;
	float radio_min;
	Vector2D centro;
	Vector2D velocidad;
	Vector2D aceleracion;
	
	Esfera();
	virtual ~Esfera();

	void Mueve(float t);
	void Dibuja();
	
	void setRadio(float r);
	void setPos(float x, float y);
	void setVel(float vx, float vy);
	
};

#endif // !defined(AFX_ESFERA_H__8D520BAF_8208_423B_BD91_29F6687FB9C3__INCLUDED_)
